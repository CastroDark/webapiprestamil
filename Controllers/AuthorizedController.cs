using System;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using webapiMongoDb.Models;
using webapiMongoDb.Service;

namespace webapiMongoDb.Controllers
{
    [Authorize]
    // [Produces("application/json")]
    //[Route("api/[controller]/[action]")]
    [ApiController]

    [Route("api/[controller]/[action]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]

    public class AuthorizedController : Controller
    {
        private readonly TokenJwtService _jwttoken;
        private readonly ILogger<AuthorizedController> _logger;
        private readonly DbContextService _dbContextService;
        private readonly ClaimsIdSession _claims;
        // private readonly IHttpContextAccessor _contextAccesor;
        public AuthorizedController(ILogger<AuthorizedController> logger,
               DbContextService dbContextService, TokenJwtService jwttoken
                )
        {
            // _contextAccesor = contextAccesor;
            _logger = logger;
            // _bookService = bookService;
            // _userService = usersService;
            _dbContextService = dbContextService;
            _jwttoken = jwttoken;

            //Get Data Session User
            var IdSession = _dbContextService.GetIdSessionClaims();
            _claims = _dbContextService.GetDataClaims(IdSession);
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Get()
        {
            return Ok(new { demo = "asd" });
        }


        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> ValidateUser([FromBody] JsonElement data)
        {

            var modelSend = new ModelJsonSendToken
            {
                dateExpire = DateTime.Now,
                msg = "Usuario Incorrecto..",
                token = ""
            };
            try
            {
                var Msg = "Usuario Incorrecto..";
                //get Model request 
                var modelRequest = JObject.Parse(data.ToString()).ToObject<UserLogin>();
                var userValidate = _dbContextService._usersService.ValidateUser(modelRequest.User, modelRequest.Password);

                if (userValidate.Password != null)
                {

                    var GetToken = new GenerateToken();
                    var IdSession = Guid.NewGuid().ToString();

                    if (userValidate.Status == 1)
                    {
                        GetToken = _jwttoken.GenetareToken(IdSession, modelRequest.Remenber);
                        await _dbContextService._IdSessionService.Generate(IdSession, userValidate.Id, GetToken);
                        _dbContextService.GetIdSessionClaims();
                        Msg = "Iniciando Session!";
                        await _dbContextService._logSystemService.AddNew(1, userValidate.Id, $"Inicio Session.. User : {userValidate.NickName}");

                        modelSend.msg = Msg;
                        modelSend.token = GetToken.Token;
                        modelSend.dateExpire = GetToken.Expiration;
                    }
                    else
                    {
                        GetToken.Token = "";
                        Msg = "Usuario Desactivado..!";
                        await _dbContextService._logSystemService.AddNew(2, userValidate.Id, $"Intento Usuario Desactivado.. User : {userValidate.NickName} ");

                        modelSend.msg = Msg;
                        modelSend.token = "";
                        modelSend.dateExpire = DateTime.Now;
                    }
                    // await _dbContextService._IdSessionService.CheckSessionUser("5e42f39b34e2d339e16fbabc");
                    // return Ok(modelSend);
                }
                await _dbContextService._logSystemService.AddNew(3, "", $"Intento Fallido.. User : {userValidate.NickName} ");

                return Ok(modelSend);

            }
            catch (System.Exception e)
            {

                _logger.LogCritical(e.ToString());
                return Ok(modelSend);
            }



        }


        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> NewUser([FromBody] JsonElement data)
        {
            return await Task.Run(async () =>
            {
                try
                {
                    //Get Json To Models
                    var modelRequest = JObject.Parse(data.ToString()).ToObject<TbUser>();
                    var ExistUser = await _dbContextService._usersService.GetUserToNickName(modelRequest.NickName);

                    if (ExistUser != null)
                    {//user Exist
                        return Ok(new { result = false, msg = "Usuario Existe.." });
                    }

                    var newUser = new TbUser
                    {
                        Create = DateTime.Now,
                        IdCompany = "222222",
                        IdRol = "",
                        ImgName = "user.jpg",
                        LastLogin = DateTime.Now,
                        LastName = "LastName1",
                        Name = "Name User#1",
                        NickName = modelRequest.NickName,
                        Note = "Note User 3",
                        Email = "correo@user.gmail.com",
                        Password = SecurePasswordHasher.Hash(modelRequest.Password),
                        Status = modelRequest.Status
                    };

                    await _dbContextService._usersService.AddNew(newUser);
                    return Ok(new { result = true, msg = "Agregado Con Exito.." });


                }
                catch (System.Exception ex)
                {
                    _logger.LogCritical(ex.ToString());
                    return Ok(new { result = false, msg = "Error en Servidor.." });
                }


            });

        }


        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> NewSettingCompany([FromBody] JsonElement data)
        {


            var SendModel = new ModelJsonSendGeneric
            {
                msg = "Error En Servidor...",
                result = false
            };

            try
            {
                //Get Json To Models
                var modelRequest = JObject.Parse(data.ToString()).ToObject<TbSettingCompany>();

                var newSetting = new TbSettingCompany
                {
                    Address = "Address",
                    DateAdd = DateTime.Now,
                    NameLong = "Nombre largo de Empresa.",
                    NameShort = "Nombre Corte",
                    Phone = "809-251-2511",
                    Slogan = "Slogan Company",
                    Status = 1
                };

                await _dbContextService._settingCompanyService.AddNew(newSetting);

                return Ok(SendModel);

            }
            catch (System.Exception ex)
            {
                _logger.LogCritical(ex.ToString());
                return Ok(SendModel);
            }




        }


        [HttpPost]
        public IActionResult Ver([FromBody] JsonElement data)
        {
            // //Recojer los Datos json en un MOdelo x 
            // var modelRequest = JObject.Parse(data.ToString()).ToObject<UserLogin>();
            // var userValidate = _dbContextService._usersService.ValidateUser(modelRequest.User, modelRequest.Password);

            // if (userValidate.Password != null)
            // {
            //     var IdSession = Guid.NewGuid().ToString();

            //     var GetToken = _jwttoken.GenetareToken(IdSession, modelRequest.Remenber);
            //     await _dbContextService._IdSessionService.Generate(IdSession, userValidate.Id, GetToken);
            //     _dbContextService.GetIdSessionClaims();
            //     // await _dbContextService._IdSessionService.CheckSessionUser("5e42f39b34e2d339e16fbabc");
            //     return Ok(new { sd = GetToken.Token });
            // }
            // var IdSession = _dbContextService.GetIdSessionClaims();
            // _logger.LogCritical(IdSession);
            // var claim = await _dbContextService.GetDataClaims(IdSession);
            return Ok(new { sd = _claims.TbRolesUser });

            //return CreatedAtRoute("GetBook", new { id = book.Id.ToString() }, book);
        }


        // [Authorize]
        // [HttpPost]
        // public async Task<IActionResult> Provar([FromBody] JsonElement data)
        // {
        //     // //Recojer los Datos json en un MOdelo x 
        //     // var modelRequest = JObject.Parse(data.ToString()).ToObject<UserLogin>();
        //     // var userValidate = _dbContextService._usersService.ValidateUser(modelRequest.User, modelRequest.Password);

        //     // if (userValidate.Password != null)
        //     // {
        //     //     var IdSession = Guid.NewGuid().ToString();

        //     //     var GetToken = _jwttoken.GenetareToken(IdSession, modelRequest.Remenber);
        //     //     await _dbContextService._IdSessionService.Generate(IdSession, userValidate.Id, GetToken);
        //     //     _dbContextService.GetClaims();
        //     //     // await _dbContextService._IdSessionService.CheckSessionUser("5e42f39b34e2d339e16fbabc");
        //     //     return Ok(new { sd = IdSession });
        //     // }

        //     var id = _dbContextService.GetClaims();
        //     _logger.LogError(id);

        //     return Ok(new { sd = Guid.NewGuid().ToString() });

        //     //return CreatedAtRoute("GetBook", new { id = book.Id.ToString() }, book);
        // }



    }
}
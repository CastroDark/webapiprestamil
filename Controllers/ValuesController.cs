using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using webapiMongoDb.Models;
using webapiMongoDb.Service;

namespace webapiMongoDb.Controllers
{


    [Authorize]
    //[Route("api/[controller]/[action]")]
    [ApiController]
    [Route("api/[controller]/[action]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]

    public class ValuesController : ControllerBase
    {
        private readonly TokenJwtService _jwttoken;
        private readonly ILogger<ValuesController> _logger;
        private readonly DbContextService _dbContextService;

        private readonly ClaimsIdSession _claims;

        private readonly FunctionsUtils _functionsUtils;
        // private readonly IHttpContextAccessor _contextAccesor;


        public ValuesController(ILogger<ValuesController> logger,
              DbContextService dbContextService, TokenJwtService jwttoken,
              ILogger<FunctionsUtils> Functlogger
               )
        {
            // _contextAccesor = contextAccesor;
            _logger = logger;
            // _bookService = bookService;
            // _userService = usersService;
            _dbContextService = dbContextService;
            _jwttoken = jwttoken;
            _functionsUtils = new FunctionsUtils(Functlogger);

            //get Data Session User
            var IdSession = _dbContextService.GetIdSessionClaims();
            _claims = _dbContextService.GetDataClaims(IdSession);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            try
            {

                return await Task.Run(() =>
                {

                    var resul = _claims.TbCompany;
                    var json = JsonConvert.SerializeObject(resul);
                    // Parse JSON into dynamic object, convenient!
                    JObject envio = JObject.Parse("{\"data\"" + ":" + json + "}");

                    return Ok(envio.ToString());
                });
                // var newCompany = new TbSettingCompany
                // {
                //     Address = "direccion de empresa c/ demo largo #1",
                //     DateAdd = DateTime.Now,
                //     NameLong = "Nombre Empresa Largo ",
                //     NameShort = "Nombre Empresa",
                //     Phone = "809-565-5844",
                //     Slogan = "slogan de empresa largo !!!",
                //     Status = 1
                // };

                // var nn = _dbContextService._settingCompanyService.AddNew(newCompany);


                // var rolNew = new TbRolesUser
                // {
                //     DateAdd = DateTime.Now,
                //     IdCompany = "5e6a39c24731a71626a4548c",
                //     IdRol = 1,
                //     IdPrivelege = "idUser",
                //     Name = "Administrador",
                //     Note = "NOtaa",
                //     Status = 1
                // };
                // var nnn = await _dbContextService._rolesUserService.AddNew(rolNew);


                // var newUser = new TbUser
                // {
                //     Create = DateTime.Now,
                //     Email = "emaildemo@gmail.com",
                //     IdCompany = "5e6a39c24731a71626a4548c",
                //     IdRol = "5e6a5a860ccd65532364b03f",
                //     ImgName = "user.png",
                //     LastLogin = DateTime.Now,
                //     Name = "Nombre Usuario",
                //     LastName = "Apellido del Usuario",
                //     NickName = "admin",
                //     Note = "NOta del usuario para demo ",
                //     Status = 1,
                //     Password = SecurePasswordHasher.Hash("231154")

                // };

                // await _dbContextService._usersService.AddNew(newUser);


            }
            catch (System.Exception)
            {

                return Ok(new { code = "Error..!" });

            }
        }


    }



}
using System;
using System.Collections;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace webapiMongoDb
{


    public class FunctionsUtils
    {
        private readonly ILogger<FunctionsUtils> _logger;
        public FunctionsUtils(ILogger<FunctionsUtils> logger)
        {
            _logger = logger;
        }

        //convert to Capitalize
        public string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1);
        }
        public string UppercaseWords(string value)
        {
            char[] array = value.ToCharArray();
            // Handle the first letter in the string.
            if (array.Length >= 1)
            {
                if (char.IsLower(array[0]))
                {
                    array[0] = char.ToUpper(array[0]);
                }
            }
            // Scan through the letters, checking for spaces.
            // ... Uppercase the lowercase letters following spaces.
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i - 1] == ' ')
                {
                    if (char.IsLower(array[i]))
                    {
                        array[i] = char.ToUpper(array[i]);
                    }
                }
            }
            return new string(array);
        }



        #region Managed Generate ID



        public async Task<double> GetUniqueIdLong()
        {
            try
            {
                return await Task.Run(() =>
          {
              Thread.Sleep(100);
              //string number = Convert.ToDateTime(DateTime.Now.AddYears(-3)).ToString("yyMMddHHmmssf");

              string number = Convert.ToDateTime(DateTime.Now).ToString("yyMMddHHmmssf");
              //_logger.LogCritical("Aqui el Error");
              // string number =String.Format("{0:d9}", (DateTime.Now.Ticks / 10) % 1000000000);
              //Thread.Sleep(100);
              return Double.Parse(number); // DateTime.Now.ToString("yyMMddHHmmssf");


          });

            }
            catch (System.Exception e)
            {
                _logger.LogCritical(e.ToString());
                return 0;
            }





        }

        //Id Unique not Repeat
        public double GetUniqueIdShort()
        {
            System.Threading.Thread.Sleep(10);

            Random generator = new Random();
            var randomgenerado = RandomNumbers(8);

            string number = randomgenerado[0].ToString();// generator.Next(0, 9).ToString("D1"); 

            var now = DateTime.Now;
            var zeroDate = DateTime.MinValue.AddHours(now.Hour).AddMinutes(now.Minute).AddSeconds(now.Second).AddMilliseconds(now.Millisecond);
            int uniqueId = (int)(zeroDate.Ticks / 10000);

            var send = number + uniqueId.ToString();

            return double.Parse(send);
        }

        public ArrayList RandomNumbers(int max)
        {
            // Create an ArrayList object that will hold the numbers
            ArrayList lstNumbers = new ArrayList();
            // The Random class will be used to generate numbers
            Random rndNumber = new Random();

            // Generate a random number between 1 and the Max
            int number = rndNumber.Next(0, max + 1);
            // Add this first random number to the list
            lstNumbers.Add(number);
            // Set a count of numbers to 0 to start
            int count = 0;

            do // Repeatedly...
            {
                // ... generate a random number between 1 and the Max
                number = rndNumber.Next(0, max + 1);

                // If the newly generated number in not yet in the list...
                if (!lstNumbers.Contains(number))
                {
                    // ... add it
                    lstNumbers.Add(number);
                }

                // Increase the count
                count++;
            } while (count <= 10 * max); // Do that again

            // Once the list is built, return it
            return lstNumbers;
        }
        #endregion

    }



}
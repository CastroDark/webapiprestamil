using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using webapiMongoDb.Helpers;
using webapiMongoDb.Models;

namespace webapiMongoDb.Service
{

    public class TokenJwtService
    {
        private readonly AppSettings _appSettings;
        public TokenJwtService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public GenerateToken GenetareToken(string IdSession, bool Remenber)
        {
            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);


            DateTime ExpireToken = DateTime.Now;
            if (Remenber)
            {
                ExpireToken = DateTime.UtcNow.AddDays(2);
            }
            else
            {
                ExpireToken = DateTime.UtcNow.AddMinutes(3);
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {

                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, IdSession)
                }),
                Expires = ExpireToken,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            //user.Token = tokenHandler.WriteToken(token);
            var send = new GenerateToken
            {
                Expiration = ExpireToken,
                Token = tokenHandler.WriteToken(token)
            };

            return send;

        }


    }

}
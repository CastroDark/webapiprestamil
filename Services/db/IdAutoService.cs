

using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Driver;
using webapiMongoDb.Models;


namespace webapiMongoDb.Service
{

    public class IdAutoService
    {
        private readonly ILogger<DbContextService> _logger;
        private readonly IMongoCollection<TbAutoIdCompany> _contextDb;
        // private readonly IMongoCollection<TbCursos> _table2;
        private readonly DbContextService _dbContextService;


        //#Managed id AutoIncremental company

        public IdAutoService(IMongoCollection<TbAutoIdCompany> tableDbContext,
         DbContextService dbContextService, ILogger<DbContextService> logger)
        {
            _contextDb = tableDbContext;
            _dbContextService = dbContextService;
            _logger = logger;
        }

        public async Task<int> GenerateId(int TypeId)
        {

            try
            {
                return await Task.Run(async () =>
               {
                   //Check Exist Index 
                   await GenerateNewIndex(TypeId);
                   var result = _contextDb.Find(x => x.TypeId == TypeId).FirstOrDefault();


                   if (result != null)
                   {
                       // IF Exist Index                    
                       var lastId = result.Sequence;
                       result.Sequence = lastId + 1;
                       result.LastUpdate = DateTime.Now;
                       _contextDb.ReplaceOne(x => x.TypeId == TypeId, result, new UpdateOptions { IsUpsert = true });

                       return lastId + 1;
                   }


                   return 0;
               });

            }
            catch (System.Exception e)
            {

                _logger.LogError(e.ToString());

                return 0;

            }





            // //await CheckSessionUser(IdUser);


        }
        private async Task<bool> GenerateNewIndex(int TypeId)
        {

            try
            {
                return await Task.Run(() =>
                {
                    var result = _contextDb.Find(x => x.TypeId == TypeId).FirstOrDefault();

                    if (result != null)
                    {
                        // IF Exist Index 
                        return true;
                    }
                    else
                    {
                        //create new Index to AutoIncrement
                        var newIdAuto = new TbAutoIdCompany
                        {
                            LastUpdate = DateTime.Now,
                            Note = $"Note {TypeId}",
                            Sequence = 0,
                            TypeId = TypeId
                        };
                        _contextDb.InsertOne(newIdAuto);

                        return true;
                    }




                });

            }
            catch (System.Exception e)
            {

                _logger.LogError(e.ToString());

                return false;

            }





            // //await CheckSessionUser(IdUser);

            // var newSession = new TbIdSession
            // {
            //     Active = true,
            //     IdSession = IdSession,
            //     Date = DateTime.Now,
            //     DateExpire = Jwt.Expiration,
            //     IdUser = IdUser,
            //     Note = "New Session ",
            //     TokenJwt = Jwt.Token
            // };

            // _contextDb.InsertOne(newSession);
            // _dbContextService._usersService.UpdateLastLogin(IdUser, DateTime.Now);

            //return newSession;
        }




    }






}
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using webapiMongoDb.Models;

namespace webapiMongoDb.Service
{



    public class SettingCompanyService
    {

        private readonly ILogger<DbContextService> _logger;
        private readonly IMongoCollection<TbSettingCompany> _contextDb;
        // private readonly IMongoCollection<TbCursos> _table2;
        private readonly DbContextService _dbContextService;


        public SettingCompanyService(IMongoCollection<TbSettingCompany> tableDbContext,
       DbContextService dbContextService, ILogger<DbContextService> logger)
        {
            _contextDb = tableDbContext;
            _dbContextService = dbContextService;
            _logger = logger;
        }


        public async Task<TbSettingCompany> AddNew(TbSettingCompany Data)
        {
            return await Task.Run(() =>
            {
                // client.Serial = SecurePasswordHasher.Hash("demoojkljhkljkljkljhkhjhblhjkbhjkhjklhbjklhbjklyuyucuicviuiuyuio");
                // DateTime t = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
                // client.Date = t.ToUniversalTime();              
                //client.Date = DateTime.Now;

                _contextDb.InsertOne(Data);

                return Data;
            });

        }
        public async Task<TbSettingCompany> FindToId(string Id)
        {
            return await Task.Run(() =>
            {
                // client.Serial = SecurePasswordHasher.Hash("demoojkljhkljkljkljhkhjhblhjkbhjkhjklhbjklhbjklyuyucuicviuiuyuio");
                // DateTime t = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
                // client.Date = t.ToUniversalTime();              
                //client.Date = DateTime.Now;

                return _contextDb.Find(x => x.Id == Id).FirstOrDefault();


            });

        }


    }
}
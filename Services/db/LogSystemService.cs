using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using webapiMongoDb.Models;

namespace webapiMongoDb.Service
{
    public class LogSystemService
    {

        //# type log 1-Imformation 
        //# type # 2 - warning
        //# type # 3 - critical

        private readonly ILogger<DbContextService> _logger;
        private readonly IMongoCollection<TbLogSystem> _contextDb;
        // private readonly IMongoCollection<TbCursos> _table2;
        private readonly DbContextService _dbContextService;
        public LogSystemService(IMongoCollection<TbLogSystem> tableDbContext,
      DbContextService dbContextService, ILogger<DbContextService> logger)
        {
            _contextDb = tableDbContext;
            _dbContextService = dbContextService;
            _logger = logger;
        }

        public async Task<TbLogSystem> AddNew(int Type, string IdUser, string Log)
        {
            return await Task.Run(() =>
            {

                var newLog = new TbLogSystem
                {
                    DateAdd = DateTime.Now,
                    Log = Log,
                    Status = 1,
                    Type = Type,
                    IdUser = IdUser
                };


                // client.Serial = SecurePasswordHasher.Hash("demoojkljhkljkljkljhkhjhblhjkbhjkhjklhbjklhbjklyuyucuicviuiuyuio");
                // DateTime t = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
                // client.Date = t.ToUniversalTime();              
                //client.Date = DateTime.Now;

                _contextDb.InsertOne(newLog);

                return newLog;
            });

        }

    }
}
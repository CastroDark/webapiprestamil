
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using webapiMongoDb.Models;

namespace webapiMongoDb.Service
{
    public class UsersService
    {


        private readonly ILogger<DbContextService> _logger;
        private readonly IMongoCollection<TbUser> _contextDb;
        // private readonly IMongoCollection<TbCursos> _table2;
        private readonly DbContextService _dbContextService;

        public UsersService(IMongoCollection<TbUser> tableDbContext,
         DbContextService dbContextService, ILogger<DbContextService> logger)
        {
            _contextDb = tableDbContext;
            _dbContextService = dbContextService;
            _logger = logger;
        }


        public async Task<TbUser> GetUserToNickName(string NickName)
        {
            return await Task.Run(() =>
            {
                var user = _contextDb.Find(x => x.NickName == NickName.ToLower()).FirstOrDefault();


                return user;
            });

        }
        public async Task<TbUser> FindToId(string Id)
        {
            return await Task.Run(() =>
            {
                var user = _contextDb.Find(x => x.Id == Id).FirstOrDefault();

                return user;
            });

        }

        public async Task<TbUser> AddNew(TbUser user)
        {
            return await Task.Run(() =>
            {
                // client.Serial = SecurePasswordHasher.Hash("demoojkljhkljkljkljhkhjhblhjkbhjkhjklhbjklhbjklyuyucuicviuiuyuio");
                // DateTime t = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
                // client.Date = t.ToUniversalTime();

                user.NickName = user.NickName.ToLower();
                DateTime s = DateTime.Parse("2020-02-07T00:36:02.126Z");
                _logger.LogInformation(s.ToString());


                //client.Date = DateTime.Now;

                _contextDb.InsertOne(user);

                return user;
            });

        }

        public TbUser ValidateUser(string NickName, string Password)
        {
            var user = new TbUser();

            var result = _contextDb.Find(x => x.NickName == NickName.ToLower()).FirstOrDefault();


            if (result != null)
            {
                //#Compare db Pass 
                var isValid = SecurePasswordHasher.Verify(Password, result.Password);

                if (isValid)
                {
                    user = result;
                }
            }


            return user;
        }

        public bool UpdateLastLogin(string IdUser, DateTime Date)
        {

            var user = _contextDb.Find(x => x.Id == IdUser).FirstOrDefault();
            user.LastLogin = Date;

            var resul = _contextDb.ReplaceOne(x => x.Id == user.Id, user);


            return true;
        }

    }
}
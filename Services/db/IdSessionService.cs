

using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Driver;
using webapiMongoDb.Models;

namespace webapiMongoDb.Service
{



    public class IdSessionService
    {
        private readonly ILogger<DbContextService> _logger;
        private readonly IMongoCollection<TbIdSession> _contextDb;
        // private readonly IMongoCollection<TbCursos> _table2;
        private readonly DbContextService _dbContextService;

        public IdSessionService(IMongoCollection<TbIdSession> tableDbContext,
         DbContextService dbContextService, ILogger<DbContextService> logger)
        {
            _contextDb = tableDbContext;
            _dbContextService = dbContextService;
            _logger = logger;
        }

        public async Task<TbIdSession> Generate(string IdSession, string IdUser, GenerateToken Jwt)
        {
            var IdSessionService = Guid.NewGuid().ToString();

            await CheckSessionUser(IdUser);

            var newSession = new TbIdSession
            {
                Active = true,
                IdSession = IdSession,
                Date = DateTime.Now,
                DateExpire = Jwt.Expiration,
                IdUser = IdUser,
                Note = "New Session ",
                TokenJwt = Jwt.Token
            };

            _contextDb.InsertOne(newSession);
            _dbContextService._usersService.UpdateLastLogin(IdUser, DateTime.Now);

            return newSession;
        }
        public async Task<TbIdSession> FindToIdSession(string Id)
        {

            try
            {

                return await Task.Run(() =>
               {
                   return _contextDb.Find(x => x.IdSession == Id).FirstOrDefault();

               });

            }
            catch (System.Exception e)
            {

                _logger.LogCritical(e.ToString());

                return new TbIdSession();

            }




        }

        public async Task<bool> CheckSessionUser(string IdUser)
        {

            return await Task.Run(() =>
            {
                try
                {
                    var result = _contextDb.Find(x => x.IdUser == IdUser & x.Active == true).FirstOrDefault();
                    if (result != null)
                    {
                        // var filter = new BsonDocument("", "Jack");
                        // var update = Builders<BsonDocument>.Update.Set("FirstName", "John");
                        result.Active = false;
                        var resultUpdate = _contextDb.ReplaceOne(x => x.Id == result.Id, result, new UpdateOptions { IsUpsert = true });
                        return true;
                    }
                    return false;
                }
                catch (System.Exception e)
                {

                    _logger.LogCritical(e.ToString());

                    return false;
                }


            });


        }


    }
}
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using webapiMongoDb.Models;

namespace webapiMongoDb.Service
{
    public class RolesUserService
    {

        //# type log 1-Imformation 
        //# type # 2 - warning
        //# type # 3 - critical

        private readonly ILogger<DbContextService> _logger;
        private readonly IMongoCollection<TbRolesUser> _contextDb;
        // private readonly IMongoCollection<TbCursos> _table2;
        private readonly DbContextService _dbContextService;
        public RolesUserService(IMongoCollection<TbRolesUser> tableDbContext,
      DbContextService dbContextService, ILogger<DbContextService> logger)
        {
            _contextDb = tableDbContext;
            _dbContextService = dbContextService;
            _logger = logger;
        }

        public async Task<TbRolesUser> AddNew(TbRolesUser rol)
        {
            return await Task.Run(() =>
            {

                // var newLog = new TbLogSystem
                // {
                //     DateAdd = DateTime.Now,
                //     Log = Log,
                //     Status = 1,
                //     Type = Type,
                //     IdUser = IdUser
                // };


                // client.Serial = SecurePasswordHasher.Hash("demoojkljhkljkljkljhkhjhblhjkbhjkhjklhbjklhbjklyuyucuicviuiuyuio");
                // DateTime t = DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy"));
                // client.Date = t.ToUniversalTime();              
                //client.Date = DateTime.Now;

                _contextDb.InsertOne(rol);

                return rol;
            });

        }

        public async Task<TbRolesUser> FindToId(string Id)
        {
            return await Task.Run(() =>
            {
                //_logger.LogCritical(_dbContextService.GetClaims());

                return _contextDb.Find(x => x.Id == Id).FirstOrDefault();

            });

        }
        public async Task<List<TbRolesUser>> GetList(string IdCompany)
        {
            return await Task.Run(() =>
            {
                //_logger.LogCritical(_dbContextService.GetClaims());

                var list = _contextDb.Find(x => x.Status == 1).ToList();
                return list;
            });

        }

    }
}
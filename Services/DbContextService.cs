using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using webapiMongoDb.Models;

namespace webapiMongoDb.Service
{

    public class DbContextService
    {

        private readonly IHttpContextAccessor _contextAccesor;
        private readonly ILogger<DbContextService> _logger;
        // private readonly BookService _bookService;

        #region  "Tables Db"
        // public readonly UserService _UserdB;

        public readonly UsersService _usersService;
        public readonly IdSessionService _IdSessionService;

        public readonly SettingCompanyService _settingCompanyService;
        public readonly LogSystemService _logSystemService;
        public readonly IdAutoService _idAutoCompanyService;

        public readonly RolesUserService _rolesUserService;
        // public readonly Cursos _cursosService;
        #endregion

        // private readonly IMongoCollection<Book> _books;

        public DbContextService(IConfigDatabase settings, ILogger<DbContextService> logger,
         IHttpContextAccessor contextAccesor)
        {     //initalize DbConections
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _logger = logger;
            _contextAccesor = contextAccesor;
            // _logger.LogCritical("hola");
            // _cursosService = new Cursos(database.GetCollection<TbCursos>("cursos"), this, this._logger);

            //Configure Table DB
            //_UserdB = new UserService(database.GetCollection<Users>("demo1"));

            _usersService = new UsersService(database.GetCollection<TbUser>("users"), this, this._logger);
            _IdSessionService = new IdSessionService(database.GetCollection<TbIdSession>("idSession"), this, this._logger);
            _settingCompanyService = new SettingCompanyService(database.GetCollection<TbSettingCompany>("settingsCompany"), this, this._logger);
            _logSystemService = new LogSystemService(database.GetCollection<TbLogSystem>("logsSystem"), this, this._logger);
            _idAutoCompanyService = new IdAutoService(database.GetCollection<TbAutoIdCompany>("idAutoCompany"), this, this._logger);
            _rolesUserService = new RolesUserService(database.GetCollection<TbRolesUser>("rolesUsers"), this, this._logger);

        }

        public string GetIdSessionClaims()
        {
            var sd = _contextAccesor.HttpContext.User;
            if (sd.Claims.Count() > 0)
            {
                return sd.Claims.FirstOrDefault().Value;
            }

            DateTime dat = DateTime.Parse("2020-02-07T00:36:02.126Z");

            return dat.ToString();
        }


        // public async Task<ClaimsIdSession> GetDataClaims(string IdSession)
        // {

        //     try
        //     {
        //         var GetTbSession = await this._IdSessionService.FindToIdSession(IdSession);
        //         var GetTbUser = await this._usersService.FindToId(GetTbSession.IdUser);

        //         var DataClaims = new ClaimsIdSession
        //         {
        //             TbSession = GetTbSession,
        //             TbUser = GetTbUser,
        //             TbCompany = await _settingCompanyService.FindToId(GetTbUser.IdCompany),
        //             TbRolesUser = await _rolesUserService.FindToId(GetTbUser.IdRol)
        //         };
        //         return DataClaims;

        //     }
        //     catch (System.Exception e)
        //     {
        //         _logger.LogCritical(e.ToString());
        //         return new ClaimsIdSession();
        //     }
        // }

        public ClaimsIdSession GetDataClaims(string IdSession)
        {

            try
            {
                var GetTbSession = this._IdSessionService.FindToIdSession(IdSession).Result;

                if (GetTbSession != null)
                {
                    var GetTbUser = this._usersService.FindToId(GetTbSession.IdUser).Result;
                    GetTbUser.Password = "";

                    var DataClaims = new ClaimsIdSession
                    {
                        TbSession = GetTbSession,
                        TbUser = GetTbUser,
                        TbCompany = _settingCompanyService.FindToId(GetTbUser.IdCompany).Result,
                        TbRolesUser = _rolesUserService.FindToId(GetTbUser.IdRol).Result
                    };
                    return DataClaims;
                }
                else
                {
                    return new ClaimsIdSession();
                }




            }
            catch (System.Exception e)
            {
                _logger.LogCritical(e.ToString());
                return new ClaimsIdSession();
            }
        }

    }

}
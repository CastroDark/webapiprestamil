using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace webapiMongoDb.Models
{
    public class TbAutoIdCompany
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public int TypeId { get; set; }
        public int Sequence { get; set; }
        public string Note { get; set; }
        [BsonDateTimeOptions]
        public DateTime LastUpdate { get; set; }
    }


}
using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace webapiMongoDb.Models
{
    public class TbSettingCompany
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        #region snippet_BookNameProperty
        // [BsonElement("Name")]
        // [JsonProperty("Name")]
        // public string Name { get; set; }
        #endregion
        public string NameLong { get; set; }
        public string NameShort { get; set; }
        public string Slogan { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public int Status { get; set; }
        public DateTime DateAdd { get; set; }

        // [BsonDateTimeOptions]
        // public DateTime Date { get; set; }
        // public DateTime DateExpire { get; set; }
        // public bool Active { get; set; }
        // public string IdUser { get; set; }
        // public string Note { get; set; }
        // public string TokenJwt { get; set; }
    }
}
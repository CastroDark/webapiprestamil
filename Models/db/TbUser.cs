using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace webapiMongoDb.Models
{
    public class TbUser
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        #region snippet_BookNameProperty
        // [BsonElement("Name")]
        // [JsonProperty("Name")]

        #endregion
        public string NickName { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string ImgName { get; set; }
        public string IdRol { get; set; }
        public int Status { get; set; }
        public string Note { get; set; }
        public string IdCompany { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        [BsonDateTimeOptions]
        public DateTime LastLogin { get; set; }
        [BsonDateTimeOptions]
        public DateTime Create { get; set; }

    }

}


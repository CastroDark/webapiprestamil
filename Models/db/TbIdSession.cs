using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace webapiMongoDb.Models
{


    public class TbIdSession
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        #region snippet_BookNameProperty
        // [BsonElement("Name")]
        // [JsonProperty("Name")]
        // public string Name { get; set; }
        #endregion
        public string IdSession { get; set; }
        [BsonDateTimeOptions]
        public DateTime Date { get; set; }
        public DateTime DateExpire { get; set; }
        public bool Active { get; set; }
        public string IdUser { get; set; }
        public string Note { get; set; }
        public string TokenJwt { get; set; }

    }
}
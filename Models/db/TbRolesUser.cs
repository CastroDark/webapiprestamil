
using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace webapiMongoDb.Models
{
    public class TbRolesUser
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public int IdRol { get; set; }
        public string IdPrivelege { get; set; }
        public string Note { get; set; }
        public string IdCompany { get; set; }
        [BsonDateTimeOptions]
        public DateTime DateAdd { get; set; }

    }
}

using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace webapiMongoDb.Models
{


    public class TbLogSystem
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        #region snippet_BookNameProperty
        // [BsonElement("Name")]
        // [JsonProperty("Name")]
        // public string Name { get; set; }
        #endregion
        public int Type { get; set; }
        public string Log { get; set; }
        public int Status { get; set; }
        public string IdUser { get; set; }
        public DateTime DateAdd { get; set; }

        // [BsonDateTimeOptions]
        // public DateTime Date { get; set; }
        // public DateTime DateExpire { get; set; }
        // public bool Active { get; set; }
        // public string IdUser { get; set; }
        // public string Note { get; set; }
        // public string TokenJwt { get; set; }
    }
}
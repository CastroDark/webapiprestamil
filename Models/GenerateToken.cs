using System;

namespace webapiMongoDb.Models
{

    public class GenerateToken
    {
        public string Token { get; set; }
        public DateTime Expiration { get; set; }

    }
}
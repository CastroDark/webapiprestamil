using System;
using System.Collections.Generic;
using webapiMongoDb.Models;

namespace webapiMongoDb
{

    public class ClaimsIdSession
    {
        public TbIdSession TbSession { get; set; }
        public TbUser TbUser { get; set; }
        public TbRolesUser TbRolesUser { get; set; }
        public TbSettingCompany TbCompany { get; set; }

    }
}
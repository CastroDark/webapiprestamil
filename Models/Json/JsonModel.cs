
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Driver;
using webapiMongoDb.Models;

namespace webapiMongoDb
{


    public class ModelJsonSendGeneric
    {
        public bool result { get; set; }
        public string msg { get; set; }
    }

    public class ModelJsonSendToken
    {
        public string token { get; set; }
        public string msg { get; set; }
        public DateTime dateExpire { get; set; }
    }

}